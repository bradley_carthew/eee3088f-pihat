# EEE3088F PiHAT
Motor driver with position feedback HAT.

How to use the PiHAT:
1 Connect the HAT to the Raspberry PiZero using the 40 pin GPIO connector.
2 Connect the voltage source to the HAT and not the Raspberry Pi Zero.
3 The LEDs indicate that the battery is on/off and if the motor is moving in forward/reverse mode.
4 Use the Raspberry Pi Zero to receive the position feedback and manipulate the motor speed/direction.
